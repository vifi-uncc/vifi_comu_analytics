import os, yaml, json, argparse, csv, re, glob
from typing import List


def extYAMLtoCSVlog(fin:str, fout:str):
	''' Extract required logs in CSV format from YAML log files. Currently, the extracted information include, for each service in one request:
	- The service name
	- The service computation time in Seconds
	- The destination IP and port (i.e., set) of each service. As a service can have multiple destinations, each destination is written in one record
	- The result name that is sent to the specified destination IP and port
	- The transfer time of the result. If the transfer time shows a negative value that is close to zero, this is because recorded sent time is done after the recorded received time, and it is safe to assume the transfer time as zero 
	@param fin: Path and name of the YAML log file 
	@type fin: str
	@param fout: Path and name of the output CSV file
	@type fout: str 
	'''
	
	with open(fin, 'r') as f:
		res = yaml.load(f,Loader=yaml.Loader)
	
	l = []	# List of CSV results
	for s in res['services']:
		for n in res['services'][s]['nifi']:
			d = {}
			d['service_name'] = s
			d['service_comp_time(sec)'] = res['services'][s]['serv_comp_time']
			d['tasks'] = res['services'][s]['tasks']
			d['dest_ip'] = n['destination']['ip']
			d['dest_port'] = n['destination']['set']
			d['result'] = n['res_file']
			d['transfer_time(sec)'] = n['transfer_time']
			l.append(d)
	
	keys = l[0].keys()
	with open(fout, 'w',newline='') as f:
		dw = csv.DictWriter(f, keys)
		dw.writeheader()
		dw.writerows(l)


def getJSONsforYAMLlogfiles(el:str,d_JSON_files:List[str])->dict:
	''' Extract paths of the JSON log files that correspond to the input YAML log file
	@param el: Path of the input YAML log file
	@type el: str
	@param d_JSON_files: List of directoreis paths that contain JSON log files
	@type d_JSON_files: List
	@return: dictionary of required paths of JSON log file corresponding to the input YAML file
	@rtype: dict
	'''
	
	JSONlogs={}	# Prepare an empty dictionary to hold the paths of the JSON log files for current request
	with open(el,'r') as f:
		l=yaml.load(f,Loader=yaml.Loader)
	# Traverse services of the current request
	for s in l['services']:
		for n in l['services'][s]['nifi']:
			# Exctract path of the JSON file for current NiFi trasfer	
			for d in d_JSON_files:
				df=glob.glob(os.path.join(d,'**',n['res_file']),recursive=True)
				if df:
					JSONlogs[n['res_file']]=df[0]
					break
	
	# Return final dictionary
	return JSONlogs

def completeLog1(logf:str, recfs:dict) -> None:
	''' Completes the YAML log file by calculating:
	- Each service computation time
	- Each result delivery and transfer time
	@param logf: The path of the YAML file to be completed
	@type logf: str
	@param recfs: dictionary of paths of JSON files containing information like time of received files at a specific VIFI Node. These file help in completing data transfer time in each YAML log file. Each dictionary key is the IP of the destination in the NiFi YAML file, while dictionary value is the path of the corresponding directory containing JSON files
	@type recfs: dict
	'''
	
	# Load the YAML log file to be completed
	with open(logf,'r') as f:
		l=yaml.load(f,Loader=yaml.Loader)
		
	# Complete the request computation time if not exists
	if 'req_comp_time' not in l or not l['req_comp_time']:
		l['req_comp_time'] = float(l['end']) - float(l['start'])
	
	# Travers services of the request
	for s in l['services']:
		itr = 0
		# Calculate computation time for each service
		l['services'][s]['serv_comp_time'] = float(l['services'][s]['end']) - float(l['services'][s]['start'])
		# Calculate transfer time for each NiFi file for current service
		for n in l['services'][s]['nifi']:
			d=recfs[n['destination']['ip']]
			with open(os.path.join(d,n['res_file']), 'r') as ff:
				fflog = json.load(ff)
			if 'deliverd_at' not in l['services'][s]['nifi'][itr] or not l['services'][s]['nifi'][itr]['deliverd_at']:
				l['services'][s]['nifi'][itr]['deliverd_at'] = float(fflog['received_at']) / 1000	# NiFi records time in milli-sec. So, time record should be devided by 1000 as other time metrics are recorded by seconds
			if 'transfer_time' not in l['services'][s]['nifi'][itr] or not l['services'][s]['nifi'][itr]['transfer_time']:
				l['services'][s]['nifi'][itr]['transfer_time'] = float(l['services'][s]['nifi'][itr]['deliverd_at']) - float(l['services'][s]['nifi'][itr]['sent'])
			itr += 1
	# Update the input YAML file
	with open(logf,'w') as f:
		yaml.dump(l,f)


def completeLog2(logf:str, recfs:dict) -> None:
	''' Completes the YAML log file by calculating:
	- Each service computation time
	- Each result delivery and transfer time
	@param logf: The path of the YAML file to be completed
	@type logf: str
	@param recfs: dictionary of paths of JSON files containing information like time of received files at a specific VIFI Node. These file help in completing data transfer time in each YAML log file. Each dictionary key is the name of the JSON file, while dictionary value is the path of the corresponding JSON file
	@type recfs: dict
	'''
	
	# Load the YAML log file to be completed
	with open(logf,'r') as f:
		l=yaml.load(f,Loader=yaml.Loader)
		
	# Complete the request computation time if not exists
	if 'req_comp_time' not in l or not l['req_comp_time']:
		l['req_comp_time'] = float(l['end']) - float(l['start'])
	
	# Travers services of the request
	for s in l['services']:
		itr = 0
		# Calculate computation time for each service
		l['services'][s]['serv_comp_time'] = float(l['services'][s]['end']) - float(l['services'][s]['start'])
		# Calculate transfer time for each NiFi file for current service
		for n in l['services'][s]['nifi']:
			with open(recfs[n['res_file']], 'r') as ff:
				fflog = json.load(ff)
			if 'deliverd_at' not in l['services'][s]['nifi'][itr] or not l['services'][s]['nifi'][itr]['deliverd_at']:
				l['services'][s]['nifi'][itr]['deliverd_at'] = float(fflog['received_at']) / 1000	# NiFi records time in milli-sec. So, time record should be devided by 1000 as other time metrics are recorded by seconds
			if 'transfer_time' not in l['services'][s]['nifi'][itr] or not l['services'][s]['nifi'][itr]['transfer_time']:
				l['services'][s]['nifi'][itr]['transfer_time'] = float(l['services'][s]['nifi'][itr]['deliverd_at']) - float(l['services'][s]['nifi'][itr]['sent'])
			itr += 1
	# Update the input YAML file
	with open(logf,'w') as f:
		yaml.dump(l,f)


def completeLog(logs_parent_path:str, d:str) -> None:
    ''' Completes the YAML log file by calculating:
    - Each service computation time
    - Each result delivery and transfer time
    @param logs_parent_path: The root (i.e., parent) directory that contains logs sub-directories. Each log sub-directory represents a VIFI Node and must be named with the IP of this VIFI Node
    @type logs_parent_path: str
    @param d: Directory name that contains the YAML log file to be completed
    @type d: str
    '''
    
    # final_log=[]
    logfs = os.listdir(os.path.join(logs_parent_path, d))
    flog = ''  # YAML log file name
    for logf in logfs:
        # Search for the YAML log file under current log directory
        if '.yml' in logf or '.yaml' in logf:
            flog = os.path.join(logs_parent_path, d, logf)
            with open(flog) as f:
                l = yaml.load(f)
                break
                
    # Complete found YAML log if exists
    if flog:
        if 'req_comp_time' not in l or not l['req_comp_time']:
            l['req_comp_time'] = float(l['end']) - float(l['start'])
        for s in l['services']:
            itr = 0
            l['services'][s]['serv_comp_time'] = float(l['services'][s]['end']) - float(l['services'][s]['start'])
            for n in l['services'][s]['nifi']:
                f = os.path.join(logs_parent_path, re.search('//(.*):', n['destination']['ip'])[1], n['res_file'])
                with open(f, 'r') as ff:
                    fflog = json.load(ff)
                if 'deliverd_at' not in l['services'][s]['nifi'][itr] or not l['services'][s]['nifi'][itr]['deliverd_at']:
                    l['services'][s]['nifi'][itr]['deliverd_at'] = float(fflog['received_at']) / 1000  # NiFi records time in milli-sec. So, time record should be devided by 1000 as other time metrics are recorded by seconds
                if 'transfer_time' not in l['services'][s]['nifi'][itr] or not l['services'][s]['nifi'][itr]['transfer_time']:
                    l['services'][s]['nifi'][itr]['transfer_time'] = float(l['services'][s]['nifi'][itr]['deliverd_at']) - float(l['services'][s]['nifi'][itr]['sent'])
                itr += 1
        # final_log.append(l)
            
        with open(flog, 'w') as f:
            yaml.dump(l, f)
    else:
        print("No YAML log file was found under specified directory " + str(os.path.join(logs_parent_path, d)) + "\n")


# Parse input arguments
#parser = argparse.ArgumentParser()
#parser.add_argument('--root_logs_path', help='Root path of the logs directory')
#parser.add_argument('--logs_dirs', nargs='+', help='List of directories, under root logs path, containing logs for each VIFI Node')  # usually, the value for this parameter is '35.167.35.244' '52.26.208.37' '52.32.117.160'

#arguments = parser.parse_args()
#logs_parent_path = arguments.root_logs_path  # Root path of the logs directory
#dirs = arguments.logs_dirs  # List of directories, under the root logs path, containing logs for each VIFI Node

## Complete each YAML log file
#for d in dirs:
#    completeLog(logs_parent_path, d)
#    # Extract required log information fromm YAML log file to a CSV file
#    logfs = os.listdir(os.path.join(logs_parent_path, d))
#    flog = ''  # YAML log file name
#    for logf in logfs:
#        # Search for the YAML log file under current log directory
#        if '.yml' in logf or '.yaml' in logf:
#            flog = os.path.join(logs_parent_path, d, logf)
#            break
#    extYAMLtoCSVlog(flog, os.path.join(logs_parent_path, d, 'summary.csv'))
# Constants

extendYAML=True	# If True, then COLLECT ALL REQUIRED LOG FILES IN ONE DIRECTORY FOR EACH EXPERIEMNT. Otherwise, COLLECT TOTAL CSV RESULTS IN 2 CSV FILES: 1) SCRIPTS 2) SERVICES

# List of experimental parameters
ps=[
{"nres":1,"res":9,"sample_prop":0.2},
{"nres":1,"res":9,"sample_prop":0.4},
{"nres":1,"res":9,"sample_prop":0.6},
{"nres":1,"res":9,"sample_prop":0.8},
{"nres":1,"res":9,"sample_prop":1},
{"nres":2,"res":9,"sample_prop":0.2},
{"nres":2,"res":9,"sample_prop":0.4},
{"nres":2,"res":9,"sample_prop":0.6},
{"nres":2,"res":9,"sample_prop":0.8},
{"nres":2,"res":9,"sample_prop":1},
{"nres":3,"res":9,"sample_prop":0.2},
{"nres":3,"res":9,"sample_prop":0.4},
{"nres":3,"res":9,"sample_prop":0.6},
{"nres":3,"res":9,"sample_prop":0.8},
{"nres":3,"res":9,"sample_prop":1},
{"nres":4,"res":9,"sample_prop":0.2},
{"nres":4,"res":9,"sample_prop":0.4},
{"nres":4,"res":9,"sample_prop":0.6},
{"nres":4,"res":9,"sample_prop":0.8},
{"nres":4,"res":9,"sample_prop":1}
]

# List of results directories
tests=[
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=1-res=9-sample_prop=0.2",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=1-res=9-sample_prop=0.4",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=1-res=9-sample_prop=0.6",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=1-res=9-sample_prop=0.8",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=1-res=9-sample_prop=1",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=2-res=9-sample_prop=0.2",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=2-res=9-sample_prop=0.4",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=2-res=9-sample_prop=0.6",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=2-res=9-sample_prop=0.8",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=2-res=9-sample_prop=1",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=3-res=9-sample_prop=0.2",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=3-res=9-sample_prop=0.4",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=3-res=9-sample_prop=0.6",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=3-res=9-sample_prop=0.8",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=3-res=9-sample_prop=1",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=4-res=9-sample_prop=0.2",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=4-res=9-sample_prop=0.4",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=4-res=9-sample_prop=0.6",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=4-res=9-sample_prop=0.8",
"/g/vifi/use_cases/JPL/ssdf/tempk/nres=4-res=9-sample_prop=1"
]
res_root_dir='/g/vifi/use_cases/JPL/ssdf/tempk/'	# Root directory containing all results
airs_JSON_logs=os.path.join(res_root_dir,'airs_logs')			# Directory containing information about all received compressed files to AIRS. Each file is a JSON file
cris_JSON_logs=os.path.join(res_root_dir,'cris_logs')			# Directory containing information about all received compressed files to CRIS. Each file is a JSON file
central_JSON_logs=os.path.join(res_root_dir,'central_logs')		# Directory containing information about all received compressed files to CENTRAL. Each file is a JSON file
#d_JSON_files=[airs_JSON_logs,cris_JSON_logs,central_JSON_logs]	# List of directories containing JSON log files
d_JSON_files={'http://cci-vifipoc-01.uncc.edu:8080/nifi/':airs_JSON_logs,'http://cci-vifipoc-04.uncc.edu:8080/nifi/':cris_JSON_logs,'http://cci-vifipoc-06.uncc.edu:8080/nifi/':central_JSON_logs}          


if extendYAML:	############ COLLECT ALL REQUIRED LOG FILES IN ONE DIRECTORY FOR EACH EXPERIEMNT ############
	for e in tests:
		# For AIRS
		el=glob.glob(os.path.join(e,'airs','ssdf_user','ssdf_user_*_log.yml'))[0]
		# Complete the YAML log file for current request
		completeLog1(el, d_JSON_files)
		# Extract required timing information in the form of CSV file
		extYAMLtoCSVlog(el, os.path.join(e,'airs','ssdf_user','airs_request_timing.csv'))
		
		# For CRIS
		el=glob.glob(os.path.join(e,'cris','ssdf_user','ssdf_user_*_log.yml'))[0]
		# Complete the YAML log file for current request
		completeLog1(el, d_JSON_files)
		# Extract required timing information in the form of CSV file
		extYAMLtoCSVlog(el, os.path.join(e,'cris','ssdf_user','cris_request_timing.csv'))
		
		# For CENTRAL
		el=glob.glob(os.path.join(e,'central','ssdf_user','ssdf_user_*_log.yml'))[0]
		# Complete the YAML log file for current request
		completeLog1(el, d_JSON_files)
		# Extract required timing information in the form of CSV file
		extYAMLtoCSVlog(el, os.path.join(e,'central','ssdf_user','central_request_timing.csv'))
else:	############ COLLECT TOTAL CSV RESULTS IN 2 CSV FILES: 1) SCRIPTS 2) SERVICES ############
	total_services_results=[]	# Hold computation time and data transfer for services
	total_scripts_results=[]	# Hold computation iime and data transfer for scripts
	for i in ps:
		e='nres='+str(i['nres'])+'-res='+str(i['res'])+'-sample_prop='+str(i['sample_prop'])
		# For CRIS
		n='cris'
		## For scripts
		s=['server_stg1_int_timing','server_EM_vifi_timing','pred_server_vifi_timing']
		for j in s:
			itr=0
			ef=os.path.join(res_root_dir,e,n,'ssdf_user',(j+'.csv'))
			with open(ef,newline='') as f:
				r=csv.DictReader(f)
				for row in r:
					row=dict(row)
					row['nres']=i['nres']
					row['res']=i['res']
					row['sample_prop']=i['sample_prop']
					row['node']=n
					row['script']=j
					row['itr']=itr
					total_scripts_results.append(row)
					itr+=1
		## For services
		sc=['cris_request_timing']
		for j in sc:
			#itr=0
			ef=os.path.join(res_root_dir,e,n,'ssdf_user',(j+'.csv'))
			with open(ef,newline='') as f:
				r=csv.DictReader(f)
				for row in r:
					row=dict(row)
					row['nres']=i['nres']
					row['res']=i['res']
					row['sample_prop']=i['sample_prop']
					row['node']=n
					#row['script']=j
					#row['itr']=itr
					total_services_results.append(row)
					#itr+=1
		# For IVE
		n='airs'
		# For scripts
		s=['server_stg1_int_timing','server_EM_vifi_timing','pred_server_vifi_timing']
		for j in s:
			itr=0
			ef=os.path.join(res_root_dir,e,n,'ssdf_user',(j+'.csv'))
			with open(ef,newline='') as f:
				r=csv.DictReader(f)
				for row in r:
					row=dict(row)
					row['nres']=i['nres']
					row['res']=i['res']
					row['sample_prop']=i['sample_prop']
					row['node']=n
					row['script']=j
					row['itr']=itr
					total_scripts_results.append(row)
					itr+=1
		## For services
		sc=['airs_request_timing']
		for j in sc:
			#itr=0
			ef=os.path.join(res_root_dir,e,n,'ssdf_user',(j+'.csv'))
			with open(ef,newline='') as f:
				r=csv.DictReader(f)
				for row in r:
					row=dict(row)
					row['nres']=i['nres']
					row['res']=i['res']
					row['sample_prop']=i['sample_prop']
					row['node']=n
					#row['script']=j
					#row['itr']=itr
					total_services_results.append(row)
					#itr+=1

		# For CENTRAL
		n='central'
		# For scripts
		s=['central_int_timing','central_pred_int_timing']
		for j in s:
			itr=0
			ef=os.path.join(res_root_dir,e,n,'ssdf_user',(j+'.csv'))
			with open(ef,newline='') as f:
				r=csv.DictReader(f)
				for row in r:
					row=dict(row)
					if 'iteration' in row.keys():
						del row['iteration']
					row['nres']=i['nres']
					row['res']=i['res']
					row['sample_prop']=i['sample_prop']
					row['node']=n
					row['script']=j
					row['itr']=itr
					total_scripts_results.append(row)
					itr+=1
		## For services
		sc=['central_request_timing']
		for j in sc:
			#itr=0
			ef=os.path.join(res_root_dir,e,n,'ssdf_user',(j+'.csv'))
			with open(ef,newline='') as f:
				r=csv.DictReader(f)
				for row in r:
					row=dict(row)
					row['nres']=i['nres']
					row['res']=i['res']
					row['sample_prop']=i['sample_prop']
					row['node']=n
					#row['script']=j
					#row['itr']=itr
					total_services_results.append(row)
					#itr+=1
					
	#print(total_scripts_results)
	keys = total_scripts_results[0].keys()
	with open(os.path.join(res_root_dir,'total_scripts_results.csv'), 'w',newline='') as f:
		dw = csv.DictWriter(f, keys)
		dw.writeheader()
		dw.writerows(total_scripts_results)	

	keys = total_services_results[0].keys()
	with open(os.path.join(res_root_dir,'total_services_results.csv'), 'w',newline='') as f:
		dw = csv.DictWriter(f, keys)
		dw.writeheader()
		dw.writerows(total_services_results)			
			
