# docker image is just temporarily needed until singularity is in effect
FROM continuumio/miniconda3

# As a precaution, upgrade and update
RUN apt update && apt upgrade -y
RUN apt install apt-transport-https ca-certificates curl gnupg lsb-release -y
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
RUN echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN apt update
RUN apt install docker-ce docker-ce-cli containerd.io -y

# Install needed libraries and modules
RUN apt install zip unzip -y && conda install python=3 -y && conda update python -y && pip install pyyaml nipyapi singularity paramiko pandas flask-restful
# && docker swarm init

# Make sure port 8080 is open and free. Otherwise, open it for NiFi. If you are going to use another port for NiFi (e.g., 8090), then open that other port (e.g., 8090) as follows:
Expose 80 8080 9443 5000 21

# Download VIFI
ENV VIFI_SRC https://melshamb@bitbucket.org/vifi-uncc/vifi_comu_analytics.git
ENV VIFI_ROOT /vifi
RUN git clone $VIFI_SRC $VIFI_ROOT
WORKDIR /vifi
#RUN cd $VIFI_ROOT && python vifi.py --vifi_conf ./vifi_config.yml &> err.log &	# This step should run after configuring VIFI server

CMD ["/bin/bash"]
